﻿using System.Linq.Expressions;

namespace Repository
{
    public interface IRepository<T>
    {
        T Get(int id);
        IQueryable<T> GetFiltered(Expression<Func<T, bool>> expressions);
        T GetAsNoTracking(int id);
        void Update(T entity);
        void Delete(int id);
        List<T> GetAll();
        void Insert(T entity);
    }
}
