﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class AplicationDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Course> Courses { get; set; }

        public AplicationDbContext(DbContextOptions<AplicationDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>()
                .HasOne(s => s.Group)
                .WithMany(g => g.Students)
                .HasForeignKey(s => s.GroupId);
            modelBuilder.Entity<Group>()
                 .HasOne(s => s.Course)
                 .WithMany(g => g.Groups)
                 .HasForeignKey(s => s.CourseId);
            modelBuilder.Entity<Course>();
        }
    }
}