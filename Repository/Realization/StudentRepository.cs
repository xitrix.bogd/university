﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Repository.Realization
{
    public class StudentRepository : IRepository<Student>
    {
        private AplicationDbContext _dbContext;

        public StudentRepository(AplicationDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));

        }

        public void Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Students.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }
            _dbContext.Students.Remove(this.Get(id));
            _dbContext.SaveChanges();

        }

        public Student Get(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Students.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }
            return _dbContext.Students.FirstOrDefault(x => x.Id == id);

        }

        public List<Student> GetAll()
        {
            return _dbContext.Students.ToList();
        }



        public Student GetAsNoTracking(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Students.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }
            return _dbContext.Students.AsNoTracking().FirstOrDefault(x => x.Id == id);

        }

        public IQueryable<Student> GetFiltered(Expression<Func<Student, bool>> expressions)
        {
            if (expressions == null)
            {
                throw new ArgumentNullException(nameof(expressions));
            }
            return _dbContext.Students.Where(expressions);
        }

        public void Insert(Student entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _dbContext.Students.Add(entity);
            _dbContext.SaveChanges();

        }

        public void Update(Student entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            _dbContext.Students.Update(entity);
            _dbContext.SaveChanges();
        }
    }
}
