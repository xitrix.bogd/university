﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Repository.Realization
{
    public class GroupRepository : IRepository<Group>
    {
        private AplicationDbContext _dbContext;

        public GroupRepository(AplicationDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public void Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Groups.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }
            _dbContext.Groups.Remove(this.Get(id));
            _dbContext.SaveChanges();

        }

        public Group Get(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Groups.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }
            return _dbContext.Groups.FirstOrDefault(x => x.Id == id);
        }

        public List<Group> GetAll()
        {
            return _dbContext.Groups.ToList();
        }



        public Group GetAsNoTracking(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Groups.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }
            return _dbContext.Groups.AsNoTracking().FirstOrDefault(x => x.Id == id);

        }

        public IQueryable<Group> GetFiltered(Expression<Func<Group, bool>> expressions)
        {
            if (expressions == null)
            {
                throw new ArgumentNullException(nameof(expressions));
            }
            return _dbContext.Groups.Where(expressions);
        }

        public void Insert(Group entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _dbContext.Groups.Add(entity);
            _dbContext.SaveChanges();

        }

        public void Update(Group entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _dbContext.Groups.Update(entity);
            _dbContext.SaveChanges();
        }
    }
}
