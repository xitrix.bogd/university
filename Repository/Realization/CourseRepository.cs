﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Data;
namespace Repository.Realization
{
    public class CourseRepository : IRepository<Course>
    {
        private readonly AplicationDbContext _dbContext;

        public CourseRepository(AplicationDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public void Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Courses.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }

            _dbContext.Courses.Remove(this.Get(id));
            _dbContext.SaveChanges();
        }

        public Course Get(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Courses.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }
            return _dbContext.Courses.FirstOrDefault(x => x.Id == id);
        }

        public List<Course> GetAll()
        {
            return _dbContext.Courses.ToList();
        }



        public Course GetAsNoTracking(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            if (!(_dbContext.Courses.AsQueryable().Any(e => e.Id == id)))
            {
                throw new KeyNotFoundException(nameof(id));
            }
            return _dbContext.Courses.AsNoTracking<Course>().FirstOrDefault(x => x.Id == id);

        }

        public IQueryable<Course> GetFiltered(Expression<Func<Course, bool>> expressions)
        {
            if (expressions == null)
            {
                throw new ArgumentNullException(nameof(expressions));
            }
            return _dbContext.Courses.Where(expressions);
        }

        public void Insert(Course entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _dbContext.Courses.Add(entity);
            _dbContext.SaveChanges();

        }

        public void Update(Course entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _dbContext.Courses.Update(entity);
            _dbContext.SaveChanges();
        }
    }
}
