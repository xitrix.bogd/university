﻿using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Service;
using Web.Models.StudentModels;
using X.PagedList;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;
namespace Web.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;
        private readonly IMapper _appMappingProfile;
        private readonly IHostingEnvironment _hostingEnvironment;

        public StudentController(IStudentService studentService, IMapper appMappingProfile, IHostingEnvironment hostingEnvironment)
        {
            _studentService = studentService ?? throw new ArgumentNullException(nameof(studentService));
            _appMappingProfile = appMappingProfile ?? throw new ArgumentNullException(nameof(appMappingProfile));
            _hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
        }
        public IActionResult Index(int? page, int? groupId)
        {
            int pageSize = 3;
            int pageNumber = 1;
            if (page != null && page > 0)
            {
                pageNumber = (int)page;
            }
            if (groupId != null)
            {
                return this.LoadIndexWithStudents(pageNumber, (int)groupId, pageSize);
            }
            return this.LoadIndexWithAllStudents(pageNumber, pageSize);
        }



        private IActionResult LoadIndexWithAllStudents(int pageNumber, int pageSize)
        {
            List<StudentViewModel> studentViewModels = _appMappingProfile.Map<List<StudentViewModel>>(_studentService.GetAll());
            StudentPagedViewModel studentPagedViewModel = new();
            studentPagedViewModel.GroupId = null;
            studentPagedViewModel.StudentViewModels = studentViewModels.ToPagedList<StudentViewModel>(pageNumber, pageSize);
            return View("Index", studentPagedViewModel);
        }

        private IActionResult LoadIndexWithStudents(int pageNumber, int groupId, int pageSize)
        {
            List<StudentViewModel> studentViewModels = _appMappingProfile.Map<List<StudentViewModel>>(_studentService.GetStudentsByGroup(groupId));
            StudentPagedViewModel studentPagedViewModel = new();
            studentPagedViewModel.GroupId = groupId;
            studentPagedViewModel.StudentViewModels = studentViewModels.ToPagedList<StudentViewModel>(pageNumber, pageSize);
            return View("Index", studentPagedViewModel);
        }

        [HttpGet]
        public IActionResult Add(int groupId)
        {
            StudentAddModel studentAddModel = new();
            studentAddModel.GroupId = groupId;
            return View("_AddFormStudents", studentAddModel);

        }


        [HttpPost]
        public IActionResult AddStudent(StudentAddModel studentAddModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", new { groupId = studentAddModel.GroupId });
            }
            Student student = _appMappingProfile.Map<Student>(studentAddModel);
            student.FilePath = this.UploadModelFileToServer(studentAddModel);
            _studentService.AddStudent(student);
            return RedirectToAction("Index", new { groupId = studentAddModel.GroupId });
        }


        public IActionResult Delete(int id)
        {
            if (!_studentService.StudentExist(id))
            {
                return RedirectToAction("Index");
            }
            Student student = _studentService.GetStudent(id);

            string filePath = student.FilePath;
            this.DeleteModelFolder(filePath);
            _studentService.RemoveStudent(id);
            return RedirectToAction("Index", new { groupId = student.GroupId });
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            if (!_studentService.StudentExist(id))
            {
                return RedirectToAction("Index");
            }
            StudentEditModel studentEditModel = _appMappingProfile.Map<StudentEditModel>(_studentService.GetStudent(id));
            return View("_EditFormStudents", studentEditModel);
        }


        [HttpPost]
        public IActionResult EditStudent(StudentEditModel studentEditModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", new { groupId = studentEditModel.GroupId });
            }
            Student student = _appMappingProfile.Map<Student>(studentEditModel);
            student.FilePath = this.EditModelFileInServer(studentEditModel);
            _studentService.UpdateStudent(student);
            return RedirectToAction("Index", new { studentEditModel.GroupId });

        }
        private string UploadModelFileToServer(StudentAddModel studentAddModel)
        {
            if (!ModelState.IsValid)
            {
                return "Model is not valit";
            }
            string path = Path.Combine(_hostingEnvironment.WebRootPath, "img", "student", $"{studentAddModel.FirstName}_{studentAddModel.LastName}");
            Directory.CreateDirectory(path);
            path = Path.Combine(path, studentAddModel.FormFile.FileName);
            using (var stream = System.IO.File.Create(path))
            {
                studentAddModel.FormFile.CopyTo(stream);
            }
            return Path.Combine(Path.AltDirectorySeparatorChar.ToString(), "img", "student", $"{studentAddModel.FirstName}_{studentAddModel.LastName}", studentAddModel.FormFile.FileName);
        }

        private string EditModelFileInServer(StudentEditModel studentEditModel)
        {
            if (!ModelState.IsValid)
            {
                return "Model is not valit";
            }
            string filePath = _studentService.GetStudentThatNoTracking(studentEditModel.Id).FilePath;

            if (studentEditModel.FormFile != null)
            {
                this.DeleteModelFolder(filePath);
                filePath = this.UploadModelFileToServer(_appMappingProfile.Map<StudentAddModel>(studentEditModel));
            }
            return filePath;
        }
        private void DeleteModelFolder(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            path = Path.GetFullPath(_hostingEnvironment.WebRootPath + path);
            if (System.IO.File.Exists(path))
            {
                Directory.Delete(Path.GetDirectoryName(path), true);
            }
        }
    }
}
