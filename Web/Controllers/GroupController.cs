﻿using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Service;
using Web.Models.GroupModels;
using X.PagedList;

namespace Web.Controllers
{
    public class GroupController : Controller
    {

        private readonly IGroupService _groupService;
        private readonly IMapper _appMappingProfile;
        public GroupController(IGroupService groupService, IMapper appMappingProfile)
        {
            _groupService = groupService ?? throw new ArgumentNullException(nameof(groupService));
            _appMappingProfile = appMappingProfile ?? throw new ArgumentNullException(nameof(appMappingProfile));
        }

        public IActionResult Index(int? page, int? courseId)
        {
            int pageSize = 3;
            int pageNumber = 1;
            if (page != null && page > 0)
            {
                pageNumber = (int)page;
            }
            if (courseId != null)
            {
                return this.LoadIndexWithGroup(pageNumber, (int)courseId, pageSize);
            }
            return this.LoadIndexWithAllGroup(pageNumber, pageSize);
        }

        private IActionResult LoadIndexWithAllGroup(int pageNumber, int pageSize)
        {
            List<GroupViewEditAddModel> groupViewModels = _appMappingProfile.Map<List<GroupViewEditAddModel>>(_groupService.GetAll());
            GroupPagedViewModel groupPagedViewModel = new();
            groupPagedViewModel.CourseId = null;
            groupPagedViewModel.GroupViewEditAddModels = groupViewModels.ToPagedList<GroupViewEditAddModel>(pageNumber, pageSize);
            return View("Index", groupPagedViewModel);
        }

        private IActionResult LoadIndexWithGroup(int pageNumber, int courseId, int pageSize)
        {
            List<GroupViewEditAddModel> groupViewModels = _appMappingProfile.Map<List<GroupViewEditAddModel>>(_groupService.GetGroupsByCourse(courseId));
            GroupPagedViewModel groupPagedViewModel = new();
            groupPagedViewModel.CourseId = courseId;
            groupPagedViewModel.GroupViewEditAddModels = groupViewModels.ToPagedList<GroupViewEditAddModel>(pageNumber, pageSize);
            return View("Index", groupPagedViewModel);
        }
        [HttpGet]
        public IActionResult Add(int courseId)
        {
            GroupViewEditAddModel groupViewAddModel = new();
            groupViewAddModel.CourseId = courseId;
            return View("_AddFormGroups", groupViewAddModel);

        }

        [HttpPost]
        public IActionResult AddGroup(GroupViewEditAddModel groupViewAddModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", new { courseId = groupViewAddModel.CourseId });
            }
            Group group = _appMappingProfile.Map<Group>(groupViewAddModel);
            _groupService.AddGroup(group);
            return RedirectToAction("Index", new { courseId = groupViewAddModel.CourseId });
        }


        public IActionResult Delete(int id)
        {
            if (!_groupService.GroupExist(id))
            {
                return RedirectToAction("Index");

            }
            Group group = _groupService.GetGroup(id);
            if (group.Students != null && group.Students.Any())
            {
                TempData.Add("Error", "First, remove the students from the group");
                return RedirectToAction("Index", new { courseId = group.CourseId });

            }
            _groupService.RemoveGroup(id);
            return RedirectToAction("Index", new { courseId = group.CourseId });
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            if (!_groupService.GroupExist(id))
            {
                return View("Index");
            }
            GroupViewEditAddModel groupViewAddModel = _appMappingProfile.Map<GroupViewEditAddModel>(_groupService.GetGroup(id));
            return View("_EditFormGroups", groupViewAddModel);
        }


        [HttpPost]
        public IActionResult EditGroup(GroupViewEditAddModel groupViewEditAddModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", new { courseId = groupViewEditAddModel.CourseId });
            }
            _groupService.UpdateGroup(_appMappingProfile.Map<Group>(groupViewEditAddModel));
            return RedirectToAction("Index", new { courseId = groupViewEditAddModel.CourseId });

        }


    }
}



