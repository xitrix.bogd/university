﻿using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Service;
using Web.Models.CourseModels;
using X.PagedList;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Web.Controllers
{
    public class CourseController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly IMapper _appMappingProfile;
        private readonly IHostingEnvironment _hostEnvironment;
        public CourseController(ICourseService courseService, IMapper appMappingProfile, IHostingEnvironment hostEnvironment)
        {
            _courseService = courseService ?? throw new ArgumentNullException(nameof(courseService));
            _appMappingProfile = appMappingProfile ?? throw new ArgumentNullException(nameof(appMappingProfile));
            _hostEnvironment = hostEnvironment ?? throw new ArgumentNullException(nameof(hostEnvironment));
        }

        public IActionResult Index(int? page)
        {
            List<CourseViewModel> courseViewModels = _appMappingProfile.Map<List<CourseViewModel>>(_courseService.GetAll());
            int pageSize = 3;
            int pageNumber = 1;
            if (page != null && page > 0)
            {
                pageNumber = (int)page;
            }

            return View(courseViewModels.ToPagedList<CourseViewModel>(pageNumber, pageSize));
        }

        public IActionResult Add()
        {
            return this.View("_AddFormCourse");
        }

        [HttpPost]
        public IActionResult AddCourse(CourseAddModel courseAddModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            Course course = _appMappingProfile.Map<Course>(courseAddModel);
            course.FilePath = this.UploadModelFileToserver(courseAddModel);
            _courseService.AddCourse(course);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            if (!_courseService.CourseExist(id))
            {
                return RedirectToAction("Index");
            }
            string filePath = _courseService.GetCourseThatNoTracking(id).FilePath;
            RemoveModelFolder(filePath);
            _courseService.RemoveCourse(id);
            return RedirectToAction("Index");

        }

        private void RemoveModelFolder(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            path = Path.GetFullPath(_hostEnvironment.WebRootPath + Path.AltDirectorySeparatorChar.ToString() + path);
            if (System.IO.File.Exists(path))
            {
                Directory.Delete(Path.GetDirectoryName(path), true);
            }
        }

        private string UploadModelFileToserver(CourseAddModel courseAddModel)
        {
            if (!ModelState.IsValid)
            {
                return "Model is not valid";
            }
            string path = Path.Combine(_hostEnvironment.WebRootPath, "img", "course", courseAddModel.Name);
            Directory.CreateDirectory(path);
            path = Path.Combine(path, courseAddModel.File.FileName);
            using (var stream = System.IO.File.Create(path))
            {
                courseAddModel.File.CopyTo(stream);
            }

            return Path.Combine("img", "course", courseAddModel.Name, courseAddModel.File.FileName);
        }


    }
}