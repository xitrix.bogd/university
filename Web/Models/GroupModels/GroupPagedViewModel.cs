﻿using X.PagedList;

namespace Web.Models.GroupModels
{
    public class GroupPagedViewModel
    {
        public int? CourseId { get; set; }

        public IPagedList<GroupViewEditAddModel> GroupViewEditAddModels { get; set; }
    }
}
