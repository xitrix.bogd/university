﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.GroupModels
{
    public class GroupViewEditAddModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(20)]
        [Display(Name = "Назва групи")]
        public string Name { get; set; }
        [Required]
        [MaxLength(200)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Опис групи")]
        public string Description { get; set; }
        [Required]
        public int CourseId { get; set; }


    }
}
