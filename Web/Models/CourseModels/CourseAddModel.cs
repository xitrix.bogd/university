﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.CourseModels
{
    public class CourseAddModel
    {
        [Required(ErrorMessage = "Pleas input name")]
        [Display(Name = "Імя")]
        public string Name { get; set; }
        [MaxLength(2000, ErrorMessage = "Lenght need be less than 2000")]
        [DataType(DataType.MultilineText)]
        [Required]
        [Display(Name = "Опис")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Логотип курсу")]
        public IFormFile File { get; set; }
    }
}
