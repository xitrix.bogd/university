﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.StudentModels
{
    public class StudentEditModel
    {
        [Required]
        public int Id { get; set; }
        [MaxLength(15)]
        [Required]
        [Display(Name = "Імя")]
        public string FirstName { get; set; }
        [MaxLength(15)]
        [Required]
        [Display(Name = "Прізвище")]
        public string LastName { get; set; }
        [Required]
        [Range(1, 100, ErrorMessage = "Please enter a value bigger than {0}")]
        [Display(Name = "Вік")]
        public int Age { get; set; }
        [Display(Name = "Фото")]
        public IFormFile? FormFile { get; set; }
        [Required]
        public int GroupId { get; set; }
    }
}
