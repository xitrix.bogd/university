﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.StudentModels
{
    public class StudentViewModel
    {
        [Required]
        public int Id { get; set; }
        [MaxLength(15)]
        [Required]
        public string FirstName { get; set; }
        [MaxLength(15)]
        [Required]

        public string LastName { get; set; }
        [Required]

        public int Age { get; set; }
        [Required]

        public string FilePath { get; set; }
        [Required]
        public int GroupId { get; set; }
    }
}
