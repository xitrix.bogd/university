﻿using X.PagedList;

namespace Web.Models.StudentModels
{
    public class StudentPagedViewModel
    {

        public int? GroupId { get; set; }

        public IPagedList<StudentViewModel> StudentViewModels;
    }
}
