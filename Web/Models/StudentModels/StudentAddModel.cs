﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.StudentModels
{
    public class StudentAddModel
    {

        [Required]
        [Display(Name = "Імя")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Прізвище")]
        public string LastName { get; set; }
        [Required]
        [Range(1, 100, ErrorMessage = "Please enter a value bigger than {0}")]
        [Display(Name = "Вік")]
        public int Age { get; set; }
        [Required]
        [Display(Name = "Фото")]
        public IFormFile FormFile { get; set; }
        [Required]
        public int GroupId { get; set; }
    }
}
