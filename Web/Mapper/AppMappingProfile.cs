﻿using AutoMapper;
using Domain;
using Web.Models.CourseModels;
using Web.Models.GroupModels;
using Web.Models.StudentModels;

namespace Web.Mapper
{
    public class AppMappingProfile : Profile
    {

        public AppMappingProfile()
        {
            CreateMap<Course, CourseViewModel>()
                .ForMember(destinationMember => destinationMember.Id, memberOptions => memberOptions.MapFrom(src => src.Id))
                .ForMember(destinationMember => destinationMember.Name, memberOptions => memberOptions.MapFrom(src => src.Name))
                .ForMember(destinationMember => destinationMember.Description, memberOptions => memberOptions.MapFrom(src => src.Description))
                .ForMember(destinationMember => destinationMember.FilePath, memberOptions => memberOptions.MapFrom(src => src.FilePath));


            CreateMap<CourseAddModel, Course>()

                .ForMember(destinationMember => destinationMember.Name, memberOptions => memberOptions.MapFrom(src => src.Name))
                .ForMember(destinationMember => destinationMember.Description, memberOptions => memberOptions.MapFrom(src => src.Description));



            CreateMap<Group, GroupViewEditAddModel>();
            CreateMap<GroupViewEditAddModel, Group>();


            CreateMap<Student, StudentViewModel>();

            CreateMap<StudentAddModel, Student>();

            CreateMap<Student, StudentEditModel>();
            CreateMap<StudentEditModel, Student>();

            CreateMap<StudentEditModel, StudentAddModel>();

        }


    }
}
