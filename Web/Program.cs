using Domain;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Realization;
using Serilog;
using Service;
using Service.Realization;
using System.Text.Json.Serialization;
using Web.Mapper;

public class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddControllersWithViews();

        string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
        builder.Services.AddDbContext<AplicationDbContext>(option => option.UseLazyLoadingProxies().UseSqlServer(connectionString));
        builder.Services.AddScoped<IRepository<Course>, CourseRepository>();
        builder.Services.AddScoped<IRepository<Group>, GroupRepository>();
        builder.Services.AddScoped<IRepository<Student>, StudentRepository>();

        builder.Services.AddScoped<ICourseService, CourseService>();
        builder.Services.AddScoped<IGroupService, GroupService>();
        builder.Services.AddScoped<IStudentService, StudentService>();
        builder.Services.AddAutoMapper(typeof(AppMappingProfile));

        builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

        var logger = new LoggerConfiguration()
          .ReadFrom.Configuration(builder.Configuration)
          .Enrich.FromLogContext()
          .CreateLogger();

        builder.Logging.ClearProviders();
        builder.Logging.AddSerilog(logger);

        var app = builder.Build();

        using (var scope = app.Services.CreateScope())
        {
            var services = scope.ServiceProvider;

            var context = services.GetRequiredService<AplicationDbContext>();
            context.Database.Migrate();
        }


        // Configure the HTTP request pipeline.
        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthorization();

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller=Course}/{action=Index}/{id?}");

        app.Run();
    }
}