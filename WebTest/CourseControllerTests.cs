using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Service;
using Web.Controllers;
using Web.Mapper;
using Web.Models.CourseModels;
using X.PagedList;

using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace WebTests
{
    public class CourseControllerTests : IDisposable
    {

        private List<Course> courses;
        private List<Course> coursesNull;
        private List<Course> coursesNotValid;

        public CourseControllerTests()
        {
            courses = new()
            {
                new Course(){Id = 1 ,Name = "NameTest1" ,Description ="DescriptionTest1",FilePath = "FilePathTest1"},
                new Course(){Id = 2 ,Name = "NameTest2" ,Description ="DescriptionTest2",FilePath = "FilePathTest2"},
                new Course(){Id = 3 ,Name = "NameTest3" ,Description ="DescriptionTest3",FilePath = "FilePathTest3"},
                new Course(){Id = 4 ,Name = "NameTest4" ,Description ="DescriptionTest4",FilePath = "FilePathTest4"},
                new Course(){Id = 5 ,Name = "NameTest5" ,Description ="DescriptionTest5",FilePath = "FilePathTest5"},
            };
            coursesNull = null;
            coursesNotValid = new()
            {
                new Course(),
                new Course (){},
                new Course (){Id=0,Name ="NameTest",Description = "DescriptionTest" ,FilePath = "FilePathTest" },
                new Course (){Id=1,Name ="NameTest",Description = "DescriptionTest" ,FilePath = "FilePathTest" },
                new Course (){Id=-1,Name ="NameTest",Description = "DescriptionTest" ,FilePath = "FilePathTest" },
                new Course (){Id=-1,Name ="",Description = "" ,FilePath = "" },
                new Course (){Id=-1,Description = "" ,FilePath = "" },
                new Course (){Id=-1,FilePath = "" },
                new Course (){Id=-1 }

            };

        }

        public void Dispose()
        {
            courses = null;
            coursesNull = null;
            coursesNotValid = null;
            try
            {
                Directory.Delete("img", true);

            }
            catch (Exception)
            {

            }
        }

        [Fact]
        public void IndexNotNullTest()
        {
            //arrange 
            var mocCourseService = new Mock<ICourseService>();
            mocCourseService.Setup(e => e.GetAll()).Returns(courses);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            CourseController homeController = new CourseController(mocCourseService.Object, mapper, mockHostingEnviromental.Object);
            //act
            ViewResult viewResult = homeController.Index(0) as ViewResult;
            //assert
            Assert.NotNull(viewResult);
        }


        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void IndexTest(int page)
        {
            //arrange 
            var mocCourseService = new Mock<ICourseService>();
            mocCourseService.Setup(e => e.GetAll()).Returns(courses);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            IPagedList<CourseViewModel> expected = mapper.Map<List<CourseViewModel>>(courses).ToPagedList<CourseViewModel>(page, 3);
            CourseController homeController = new CourseController(mocCourseService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = homeController.Index(page);
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<CourseViewModel>>(viewResult.Model);
            //assert
            Assert.Equal(expected.Count, model.Count());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void IndexCoursesNullTest(int page)
        {
            //arrange 
            var mocCourseService = new Mock<ICourseService>();
            mocCourseService.Setup(e => e.GetAll()).Returns(coursesNull);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            IPagedList<CourseViewModel> expected = mapper.Map<List<CourseViewModel>>(coursesNull).ToPagedList<CourseViewModel>(page, 3);
            CourseController homeController = new CourseController(mocCourseService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = homeController.Index(page);
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<CourseViewModel>>(viewResult.Model);
            //assert
            Assert.Equal(expected.Count, model.Count());
        }


        [Fact]
        public void AddTest()
        {
            //arrange
            var mocCourseService = new Mock<ICourseService>();
            mocCourseService.Setup(e => e.GetAll()).Returns(coursesNull);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            CourseController homeController = new CourseController(mocCourseService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = homeController.Add() as ViewResult;
            //assert
            Assert.Equal("_AddFormCourse", result.ViewName);
        }

        [Fact]
        public void AddValidCourseTest()
        {
            //arrange
            var mocCourseService = new Mock<ICourseService>();
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            mockHostingEnviromental.Setup(e => e.WebRootPath).Returns("");
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();

            CourseController homeController = new CourseController(mocCourseService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var accually = new List<String>();
            foreach (var item in courses)
            {
                var result = (RedirectToActionResult)homeController.AddCourse(this.Map(item));
                accually.Add(result.ActionName);

            }
            //assert
            foreach (var item in accually)
            {
                Assert.Equal("Index", item);
            }
        }

        [Fact]
        public void AddNotValidCourseTest()
        {
            //arrange
            var mocCourseService = new Mock<ICourseService>();
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            mockHostingEnviromental.Setup(e => e.WebRootPath).Returns("");
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            CourseController homeController = new CourseController(mocCourseService.Object, mapper, mockHostingEnviromental.Object);
            homeController.ModelState.AddModelError(nameof(CourseAddModel.Name), "Required");
            homeController.ModelState.AddModelError(nameof(CourseAddModel.Description), "Required");
            homeController.ModelState.AddModelError(nameof(CourseAddModel.File), "Required");
            //act
            var accually = new List<String>();
            foreach (var item in coursesNotValid)
            {
                var result = (RedirectToActionResult)homeController.AddCourse(this.Map(item));
                accually.Add(result.ActionName);

            }
            //assert
            foreach (var item in accually)
            {
                Assert.Equal("Index", item);
            }
        }


        private CourseAddModel Map(Course course)
        {
            CourseAddModel courseAddModel = new();
            courseAddModel.Name = course.Name;
            courseAddModel.Description = course.Description;
            var file = new Mock<IFormFile>();
            file.Setup(e => e.CopyTo(It.IsAny<Stream>()));
            file.Setup(e => e.FileName).Returns("tempName");
            courseAddModel.File = file.Object;
            return courseAddModel;
        }


        [Fact]
        public void DeleteCourseNotExistTest()
        {
            //arrange

            var mocCourseService = new Mock<ICourseService>();
            mocCourseService.Setup(e => e.CourseExist(It.IsAny<int>())).Returns(false);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            CourseController homeController = new CourseController(mocCourseService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var acctual = homeController.Delete(It.IsAny<int>()) as RedirectToActionResult;

            //assert
            Assert.Equal("Index", acctual.ActionName);
        }

        [Fact]
        public void DeleteCourseThatExistTest()
        {
            //arrange
            var mocCourseService = new Mock<ICourseService>();
            mocCourseService.Setup(e => e.CourseExist(It.IsAny<int>())).Returns(true);
            mocCourseService.Setup(e => e.GetCourseThatNoTracking(It.IsAny<int>())).Returns(new Course() { FilePath = "test" });
            mocCourseService.Setup(e => e.RemoveCourse(It.IsAny<int>()));
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            CourseController homeController = new CourseController(mocCourseService.Object, mapper, mockHostingEnviromental.Object);
            //act

            var acctual = homeController.Delete(1) as RedirectToActionResult;

            //assert
            Assert.Equal("Index", acctual.ActionName);
        }
    }
}