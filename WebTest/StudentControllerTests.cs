﻿using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Service;
using Web.Controllers;
using Web.Mapper;
using Web.Models.StudentModels;
using X.PagedList;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace WebTest
{
    public class StudentControllerTests : IDisposable
    {


        private readonly List<Student> students;

        public StudentControllerTests()
        {
            students = new List<Student>()
            {
                new Student(){Id = 1 , FirstName = "FirstNameTest1" , LastName = "LastNameTest1" ,Age = 1 ,FilePath = "FilePathTest"},
                new Student(){Id = 2 , FirstName = "FirstNameTest2" , LastName = "LastNameTest2" ,Age = 2 ,FilePath = "FilePathTest"},
                new Student(){Id = 3 , FirstName = "FirstNameTest3" , LastName = "LastNameTest3" ,Age = 3 ,FilePath = "FilePathTest"},
                new Student(){Id = 4 , FirstName = "FirstNameTest4" , LastName = "LastNameTest4" ,Age = 4 ,FilePath = "FilePathTest"},
            };
        }

        public void Dispose()
        {
            try
            {
                Directory.Delete("img", true);

            }
            catch (Exception)
            {

            }

        }

        [Fact]
        public void IndexNotNullTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.GetAll()).Returns(students);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();

            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            //act
            ViewResult viewResult = studentController.Index(0, 1) as ViewResult;
            //assert
            Assert.NotNull(viewResult);
        }

        [Fact]
        public void IndexTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.GetAll()).Returns(students);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();

            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            IPagedList<StudentViewModel> expected = mapper.Map<List<StudentViewModel>>(students).ToPagedList<StudentViewModel>(1, 3);
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = studentController.Index(1, null);
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsType<StudentPagedViewModel>(viewResult.Model);
            //assert
            Assert.Equal(expected.Count, model.StudentViewModels.Count);
        }


        [Fact]
        public void AddTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.GetAll()).Returns(students);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();

            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = studentController.Add(1) as ViewResult;
            //assert
            Assert.Equal("_AddFormStudents", result.ViewName);
        }



        [Fact]
        public void AddStudentNotValidTests()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.GetAll()).Returns(students);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();

            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            Student student = new();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            studentController.ModelState.AddModelError(nameof(StudentAddModel.FirstName), "Required");
            studentController.ModelState.AddModelError(nameof(StudentAddModel.LastName), "Required");
            studentController.ModelState.AddModelError(nameof(StudentAddModel.Age), "Required");
            studentController.ModelState.AddModelError(nameof(StudentAddModel.FormFile), "Required");
            //act
            var result = studentController.AddStudent(this.Map(student)) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);
        }


        private StudentAddModel Map(Student student)
        {
            StudentAddModel studentAddModel = new();
            studentAddModel.FirstName = student.FirstName;
            studentAddModel.LastName = student.LastName;
            studentAddModel.Age = student.Age;
            studentAddModel.GroupId = student.GroupId;
            var file = new Mock<IFormFile>();
            file.Setup(e => e.CopyTo(It.IsAny<Stream>()));
            file.Setup(e => e.FileName).Returns("tempName");
            studentAddModel.FormFile = file.Object;
            return studentAddModel;
        }


        [Fact]
        public void AddStudentTests()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            mockHostingEnviromental.Setup(e => e.WebRootPath).Returns("");

            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            List<string> accually = new List<string>();
            //act
            foreach (var item in students)
            {
                var result = studentController.AddStudent(this.Map(item)) as RedirectToActionResult;
                accually.Add(result.ActionName);
            }
            //assert
            foreach (var item in accually)
            {
                Assert.Equal("Index", item);
                mocStudentService.Verify(e => e.AddStudent(It.IsAny<Student>()), Times.Exactly(accually.Count));
            }
        }


        [Fact]
        public void DeleteStudentNotExistTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.StudentExist(It.IsAny<int>())).Returns(false);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();

            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = studentController.Delete(0) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);
        }

        [Fact]
        public void DeleteStudentTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.StudentExist(It.IsAny<int>())).Returns(true);
            mocStudentService.Setup(e => e.GetStudent(It.IsAny<int>())).Returns(new Student() { FilePath = "" });
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();

            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = studentController.Delete(0) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);
            mocStudentService.Verify(e => e.RemoveStudent(It.IsAny<int>()), Times.Once);
        }


        [Fact]
        public void EditStudentNotExistTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.StudentExist(It.IsAny<int>())).Returns(false);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = studentController.Edit(1) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);
        }

        [Fact]
        public void EditTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.StudentExist(It.IsAny<int>())).Returns(true);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = studentController.Edit(1) as ViewResult;
            //assert
            Assert.Equal("_EditFormStudents", result.ViewName);
        }

        [Fact]
        public void EditStudentNotValitTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.StudentExist(It.IsAny<int>())).Returns(true);
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            Student student = new();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            studentController.ModelState.AddModelError(nameof(StudentEditModel.Id), "Required");
            //act
            var result = studentController.EditStudent(mapper.Map<StudentEditModel>(student)) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);
        }

        [Fact]
        public void EditStudentTest()
        {
            //arrange 
            var mocStudentService = new Mock<IStudentService>();
            mocStudentService.Setup(e => e.StudentExist(It.IsAny<int>())).Returns(true);
            mocStudentService.Setup(e => e.GetStudentThatNoTracking(It.IsAny<int>())).Returns(new Student() { FilePath = "" });
            var mockHostingEnviromental = new Mock<IHostingEnvironment>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            Student student = new();
            StudentController studentController = new StudentController(mocStudentService.Object, mapper, mockHostingEnviromental.Object);
            //act
            var result = studentController.EditStudent(mapper.Map<StudentEditModel>(student)) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);
            mocStudentService.Verify(e => e.UpdateStudent(It.IsAny<Student>()), Times.Once);
        }

    }
}
