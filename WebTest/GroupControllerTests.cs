﻿using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using Service;
using Web.Controllers;
using Web.Mapper;
using Web.Models.GroupModels;
using X.PagedList;

namespace WebTest
{
    public class GroupControllerTests
    {

        private List<Group> groups;
        private List<Group> groupsNotValid;


        public GroupControllerTests()
        {

            groups = new List<Group>()
            {
                new Group (){Id=1, Name= "Name1" , Description = "Desc1" , CourseId = 1},
                new Group (){Id=2, Name= "Name2" , Description = "Desc2" , CourseId = 1},
                new Group (){Id=3, Name= "Name3" , Description = "Desc3" , CourseId = 1},
                new Group (){Id=4, Name= "Name4" , Description = "Desc4" , CourseId = 1}
            };
            groupsNotValid = new List<Group>()
            {
                new Group(),
                new Group(){Id = 0,Name = null , Description = null , CourseId = 0},
                new Group(){Id = 0,Name = null , Description = null },
                new Group(){Id = 0,Name = null },
                new Group(){Id = 0}
            };

        }

        [Fact]
        public void IndexNotNullTest()
        {
            //arrange 
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.GetAll()).Returns(groups);
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            //act
            ViewResult viewResult = groupController.Index(0, 1) as ViewResult;
            //assert
            Assert.NotNull(viewResult);
        }

        [Fact]
        public void IndexTest()
        {
            //arrange 
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.GetAll()).Returns(groups);
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            IPagedList<GroupViewEditAddModel> expected = mapper.Map<List<GroupViewEditAddModel>>(groups).ToPagedList<GroupViewEditAddModel>(1, 3);
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            //act
            var result = groupController.Index(1, null);
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsType<GroupPagedViewModel>(viewResult.Model);
            //assert
            Assert.Equal(expected.Count, model.GroupViewEditAddModels.Count);
        }



        [Fact]
        public void AddTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            //act
            var result = groupController.Add(1) as ViewResult;
            //assert
            Assert.Equal("_AddFormGroups", result.ViewName);
        }

        [Fact]
        public void AddGroupValidTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.AddGroup(It.IsAny<Group>()));
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            List<string> accuallyViewName = new List<string>();
            //act
            foreach (var item in groups)
            {
                var result = groupController.AddGroup(mapper.Map<GroupViewEditAddModel>(item)) as RedirectToActionResult;
                accuallyViewName.Add(result.ActionName);
            }
            //assert
            foreach (var item in accuallyViewName)
            {
                Assert.Equal("Index", item);
            }
        }
        [Fact]
        public void AddGroupNotValidTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.AddGroup(It.IsAny<Group>()));
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            List<string> accuallyViewName = new List<string>();
            //act
            foreach (var item in groupsNotValid)
            {
                var result = groupController.AddGroup(mapper.Map<GroupViewEditAddModel>(item)) as RedirectToActionResult;
                accuallyViewName.Add(result.ActionName);
            }
            //assert
            foreach (var item in accuallyViewName)
            {
                Assert.Equal("Index", item);
            }
        }


        [Fact]
        public void DeleteGroupNotExist()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.GroupExist(It.IsAny<int>())).Returns(false);
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            //act
            var result = groupController.Delete(0) as RedirectToActionResult;
            //
            Assert.Equal("Index", result.ActionName);
        }

        [Fact]
        public void DeleteGroupThatHasStudentTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            var mockTempData = new Mock<ITempDataDictionary>();
            mocGroupService.Setup(e => e.GroupExist(It.IsAny<int>())).Returns(true);
            Student student = new();
            Group group = new() { Id = 1, Name = "TestName", Description = "TestDesc", CourseId = 0, Students = new List<Student> { student } };
            mocGroupService.Setup(e => e.GetGroup(It.IsAny<int>())).Returns(group);
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            groupController.TempData = mockTempData.Object;
            //act
            var result = groupController.Delete(0) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);

        }

        [Fact]
        public void DeleteGroupIsValidTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.GroupExist(It.IsAny<int>())).Returns(true);
            Group group = new() { Id = 1, Name = "TestName", Description = "TestDesc", CourseId = 0 };
            mocGroupService.Setup(e => e.GetGroup(It.IsAny<int>())).Returns(group);
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            //act
            var result = groupController.Delete(0) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);
            mocGroupService.Verify(e => e.RemoveGroup(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void EditGroupNotExistTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.GroupExist(It.IsAny<int>())).Returns(false);
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            //act
            var result = groupController.Edit(0) as ViewResult;
            //assert
            Assert.Equal("Index", result.ViewName);
        }

        [Fact]
        public void EditGroupTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.GroupExist(It.IsAny<int>())).Returns(true);
            mocGroupService.Setup(e => e.GetGroup(It.IsAny<int>()));
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            //act
            var result = groupController.Edit(0) as ViewResult;
            //assert
            Assert.Equal("_EditFormGroups", result.ViewName);
        }

        [Fact]
        public void EditGroupNotValidTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.GroupExist(It.IsAny<int>())).Returns(true);
            mocGroupService.Setup(e => e.GetGroup(It.IsAny<int>()));
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            Group groupNotValid = new() { };
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            groupController.ModelState.AddModelError(nameof(GroupViewEditAddModel.Id), "Required");
            groupController.ModelState.AddModelError(nameof(GroupViewEditAddModel.Name), "Required");
            groupController.ModelState.AddModelError(nameof(GroupViewEditAddModel.Description), "Required");
            groupController.ModelState.AddModelError(nameof(GroupViewEditAddModel.CourseId), "Required");
            //act
            var result = groupController.EditGroup(mapper.Map<GroupViewEditAddModel>(groupNotValid)) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);

        }

        [Fact]
        public void EditGroupValidTest()
        {
            //arrange
            var mocGroupService = new Mock<IGroupService>();
            mocGroupService.Setup(e => e.GroupExist(It.IsAny<int>())).Returns(true);
            mocGroupService.Setup(e => e.GetGroup(It.IsAny<int>()));
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AppMappingProfile>();
            }).CreateMapper();
            Group groupValid = new() { Id = 1, Name = "Test", Description = "Test", CourseId = 1 };
            GroupController groupController = new GroupController(mocGroupService.Object, mapper);
            //act
            var result = groupController.EditGroup(mapper.Map<GroupViewEditAddModel>(groupValid)) as RedirectToActionResult;
            //assert
            Assert.Equal("Index", result.ActionName);
            mocGroupService.Verify(e => e.UpdateGroup(It.IsAny<Group>()), Times.Once);

        }

    }
}
