﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Student
    {
        [Key]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }
        public string FilePath { get; set; }
        [ForeignKey(nameof(Group))]

        public int GroupId { get; set; }
        public virtual Group Group { get; set; }
    }
}
