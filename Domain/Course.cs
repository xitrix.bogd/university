﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Course
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string FilePath { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
    }
}