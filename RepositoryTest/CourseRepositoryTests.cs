using Domain;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Realization;

namespace RepositoryTest
{
    public class CourseRepositoryTests : IDisposable
    {

        private readonly SqliteConnection _connection;
        private readonly AplicationDbContext _context;
        private string samePathForTest = "FilePath";
        public CourseRepositoryTests()
        {
            _connection = new SqliteConnection("Data Source=:memory:");
            _connection.Open();

            var _dbContextOptions = new DbContextOptionsBuilder<AplicationDbContext>()
             .UseSqlite(_connection)
             .Options;

            _context = new AplicationDbContext(_dbContextOptions);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _context.AddRange(
                    new Course { Id = 1, Name = "Test1", Description = "Description1", FilePath = "FilePath1" },
                    new Course { Id = 2, Name = "Test2", Description = "Description2", FilePath = samePathForTest },
                    new Course { Id = 3, Name = "Test3", Description = "Description3", FilePath = samePathForTest }

                    );
            _context.SaveChanges();
        }


        public void Dispose()
        {
            _connection.Dispose();
            _context.Dispose();
        }




        [Fact]
        public void CourseRepositoryExeptionTest()
        {
            //arrange

            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => new CourseRepository(null));
        }

        [Fact]
        public void DeleteExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Delete(0));


        }

        [Fact]
        public void DeleteExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            //act
            //assert
             Assert.Throws<KeyNotFoundException>(() => repository.Delete(_context.Courses.Count() + 1));
        }

        [Fact]
        public void DeleteTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            Course course = _context.Courses.First();
            //act
            repository.Delete(course.Id);
            var accually = _context.Courses.First();
            //assert
            Assert.NotNull(accually);
            Assert.NotEqual(accually, course);

        }


        [Fact]
        public void GetExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Get(0));

        }

        [Fact]
        public void GetExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            //act
            //assert
            Assert.Throws<KeyNotFoundException>(() => repository.GetAsNoTracking(_context.Courses.Count() + 1));

        }

        [Fact]
        public void GetTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            var id = _context.Courses.Count();
            //act
            Course accuallyCourse = repository.Get(id);
            //assert
            Assert.NotNull(accuallyCourse);
            Assert.Equal(_context.Courses.FirstOrDefault(el => el.Id == id), accuallyCourse);
        }



        [Fact]
        public void GetAllTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            //act
            List<Course> accually = repository.GetAll();
            //assert
            Assert.NotNull(accually);
            Assert.Equal(_context.Courses.Count(), accually.Count);
        }



        [Fact]
        public void GetAsNoTrackingExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.GetAsNoTracking(0));
        }

        [Fact]
        public void GetAsNoTrackingExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            //act
            //assert
            Assert.Throws<KeyNotFoundException>(() => repository.GetAsNoTracking(_context.Courses.Count() + 1));

        }

        [Fact]
        public void GetAsNoTrackingtTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            var id = _context.Courses.Count() - 1;
            //act
            Course accuallyCourse = repository.GetAsNoTracking(id);
            Course expected = _context.Courses.AsNoTracking().FirstOrDefault(el => el.Id == id);
            //assert
            Assert.NotNull(accuallyCourse);
            Assert.Equal(expected.Name, accuallyCourse.Name);
            Assert.Equal(expected.Description, accuallyCourse.Description);
            Assert.Equal(expected.Id, accuallyCourse.Id);
            Assert.Equal(expected.FilePath, accuallyCourse.FilePath);
        }



        [Fact]
        public void GetFilteredExeptionTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => repository.GetFiltered(null));
        }

        [Fact]
        public void GetFilteredTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);

            //act
            IQueryable<Course> accuallyCourses = repository.GetFiltered(expressions: e => e.FilePath == samePathForTest);
            IQueryable<Course> expected = _context.Courses.AsQueryable().Where(e => e.FilePath == samePathForTest);
            //assert
            Assert.NotNull(accuallyCourses);
            Assert.Equal(expected, accuallyCourses);
        }

        [Fact]
        public void InsertExeptionTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            Course course = null;
            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => repository.Insert(course));
        }

        [Fact]
        public void InsertTest()
        {
            //arange
            IRepository<Course> repository = new CourseRepository(_context);
            Course course = new Course() { Id = _context.Courses.Count() + 1, Name = "NameAdd", Description = "DescriptionAdd", FilePath = "FilePathAdd" };
            //act
            repository.Insert(course);
            Course accually = repository.Get(_context.Courses.Count());
            //assert
            Assert.NotNull(accually);
            Assert.Equal(course, accually);
        }


        [Fact]
        public void UpdateExeptionTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            Course course = null;
            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => repository.Update(course));
        }

        [Fact]
        public void UpdateTest()
        {
            //arrange
            IRepository<Course> repository = new CourseRepository(_context);
            Course expectedCourse = repository.Get(_context.Courses.Count());
            expectedCourse.Name = "UpdatedName";
            //act
            repository.Update(expectedCourse);
            Course actualCourse = repository.GetAsNoTracking(_context.Courses.Count());
            //assert
            Assert.NotNull(actualCourse);
            Assert.Equal(expectedCourse.Name, actualCourse.Name);
        }

    }
}