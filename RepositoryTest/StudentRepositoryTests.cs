﻿using Domain;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Realization;

namespace RepositoryTests
{
    public class StudentRepositoryTests : IDisposable
    {
        private readonly SqliteConnection _connection;
        private readonly AplicationDbContext _context;
        private const int sameGroupIdForTest = 2;
        public StudentRepositoryTests()
        {
            _connection = new SqliteConnection("Data Source=:memory:");
            _connection.Open();

            var _dbContextOptions = new DbContextOptionsBuilder<AplicationDbContext>()
             .UseSqlite(_connection)
             .Options;

            _context = new AplicationDbContext(_dbContextOptions);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _context.AddRange(
                    new Course { Id = 1, Name = "Name1", Description = "Desc1", FilePath = " FilePath1" },
                    new Course { Id = 2, Name = "Name2", Description = "Desc2", FilePath = " FilePath2" },
                    new Group { Id = 1, Name = "Test1", Description = "Description1", CourseId = 1 },
                    new Group { Id = 2, Name = "Test2", Description = "Description2", CourseId = 2 },
                    new Student { Id = 1, FirstName = "FirstName1", LastName = "LastName1", Age = 1, FilePath = "FilePath", GroupId = 1 },
                    new Student { Id = 2, FirstName = "FirstName2", LastName = "LastName2", Age = 1, FilePath = "FilePath", GroupId = 2 },
                    new Student { Id = 3, FirstName = "FirstName3", LastName = "LastName3", Age = 1, FilePath = "FilePath", GroupId = 2 }

                    );
            _context.SaveChanges();
        }


        public void Dispose()
        {
            _connection.Dispose();
            _context.Dispose();
        }

        [Fact]
        public void StudentRepositoryExeptionTest()
        {
            //arrange

            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => new StudentRepository(null));
        }

        [Fact]
        public void DeleteExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Delete(0));


        }

        [Fact]
        public void DeleteExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            //assert
            Assert.Throws<KeyNotFoundException>(() => repository.Delete(_context.Students.Count() + 1));


        }

        [Fact]
        public void DeleteTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            Student student = _context.Students.First();
            //act
            repository.Delete(student.Id);
            var accually = _context.Students.First();
            //assert
            Assert.NotNull(accually);
            Assert.NotEqual(accually, student);

        }


        [Fact]
        public void GetExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Get(0));

        }

        [Fact]
        public void GetExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            //assert
            Assert.Throws<KeyNotFoundException>(() => repository.Get(_context.Students.Count() + 1));

        }

        [Fact]
        public void GetTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            var id = _context.Students.Count();
            //act
            Student accuallyStudent = repository.Get(id);
            //assert
            Assert.NotNull(accuallyStudent);
            Assert.Equal(_context.Students.FirstOrDefault(el => el.Id == id), accuallyStudent);
        }



        [Fact]
        public void GetAllTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            List<Student> accually = repository.GetAll();
            //assert
            Assert.NotNull(accually);
            Assert.Equal(_context.Students.Count(), accually.Count);
        }



        [Fact]
        public void GetAsNoTrackingExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.GetAsNoTracking(0));

        }

        [Fact]
        public void GetAsNoTrackingExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            //assert
            Assert.Throws<KeyNotFoundException>(() => repository.GetAsNoTracking(_context.Students.Count() + 1));

        }

        [Fact]
        public void GetAsNoTrackingtTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            var id = _context.Students.Count() - 1;
            //act
            Student accuallyStudent = repository.GetAsNoTracking(id);
            Student expected = _context.Students.AsNoTracking().FirstOrDefault(el => el.Id == id);
            //assert
            Assert.NotNull(accuallyStudent);
            Assert.Equal(expected.FirstName, accuallyStudent.FirstName);
            Assert.Equal(expected.LastName, accuallyStudent.LastName);
            Assert.Equal(expected.Age, accuallyStudent.Age);
            Assert.Equal(expected.Id, accuallyStudent.Id);
            Assert.Equal(expected.GroupId, accuallyStudent.GroupId);
        }



        [Fact]
        public void GetFilteredExeptionTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => repository.GetFiltered(null));
        }

        [Fact]
        public void GetFilteredTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            //act
            IQueryable<Student> accuallyStudents = repository.GetFiltered(expressions: e => e.GroupId == sameGroupIdForTest);
            IQueryable<Student> expected = _context.Students.AsQueryable().Where(e => e.GroupId == sameGroupIdForTest);
            //assert
            Assert.NotNull(accuallyStudents);
            Assert.Equal(expected, accuallyStudents);
        }

        [Fact]
        public void InsertExeptionTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            Student student = null;
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => repository.Insert(student));
        }

        [Fact]
        public void InsertTest()
        {
            //arange
            IRepository<Student> repository = new StudentRepository(_context);
            Student student = new Student() { Id = _context.Students.Count() + 1, FirstName = "FirstNameAdd", LastName = "LastNameAdd", Age = 1, FilePath = "FilePath", GroupId = 1 };
            //act
            repository.Insert(student);
            Student accually = repository.Get(_context.Students.Count());
            //assert
            Assert.NotNull(accually);
            Assert.Equal(student, accually);
        }


        [Fact]
        public void UpdateExeptionTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            Student student = null;
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => repository.Update(student));
        }

        [Fact]
        public void UpdateTest()
        {
            //arrange
            IRepository<Student> repository = new StudentRepository(_context);
            Student expectedStudent = repository.Get(_context.Students.Count());
            expectedStudent.FirstName = "UpdatedName";
            //act
            repository.Update(expectedStudent);
            Student actualStudent = repository.GetAsNoTracking(_context.Students.Count());
            //assert
            Assert.NotNull(actualStudent);
            Assert.Equal(expectedStudent.FirstName, actualStudent.FirstName);
        }
    }
}
