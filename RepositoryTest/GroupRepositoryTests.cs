﻿using Domain;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Realization;

namespace RepositoryTests
{
    public class GroupRepositoryTests : IDisposable
    {
        private readonly SqliteConnection _connection;
        private readonly AplicationDbContext _context;
        private const int sameCourseIdForTest = 2;
        public GroupRepositoryTests()
        {
            _connection = new SqliteConnection("Data Source=:memory:");
            _connection.Open();

            var _dbContextOptions = new DbContextOptionsBuilder<AplicationDbContext>()
             .UseSqlite(_connection)
             .Options;

            _context = new AplicationDbContext(_dbContextOptions);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _context.AddRange(
                    new Course { Id = 1, Name = "Name1", Description = "Desc1", FilePath = " FilePath1" },
                    new Course { Id = 2, Name = "Name2", Description = "Desc2", FilePath = " FilePath2" },
                    new Group { Id = 1, Name = "Test1", Description = "Description1", CourseId = 1 },
                    new Group { Id = 2, Name = "Test2", Description = "Description2", CourseId = sameCourseIdForTest },
                    new Group { Id = 3, Name = "Test3", Description = "Description3", CourseId = sameCourseIdForTest }

                    );
            _context.SaveChanges();
        }


        public void Dispose()
        {
            _connection.Dispose();
            _context.Dispose();
        }




        [Fact]
        public void GroupRepositoryExeptionTest()
        {
            //arrange

            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => new GroupRepository(null));
        }

        [Fact]
        public void DeleteExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            //act
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Delete(0));
            //assert

        }

        [Fact]
        public void DeleteExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            //act
            //assert
            Assert.Throws<KeyNotFoundException>(() => repository.Delete(_context.Groups.Count() + 1));

        }

        [Fact]
        public void DeleteTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            Group group = _context.Groups.First();
            //act
            repository.Delete(group.Id);
            var accually = _context.Groups.First();
            //assert
            Assert.NotNull(accually);
            Assert.NotEqual(accually, group);

        }


        [Fact]
        public void GetExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Get(0));

        }

        [Fact]
        public void GetExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            //act
            //assert
            Assert.Throws<KeyNotFoundException>(() => repository.Get(_context.Groups.Count() + 1));

        }

        [Fact]
        public void GetTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            var id = _context.Groups.Count();
            //act
            Group accuallyGroup = repository.Get(id);
            //assert
            Assert.NotNull(accuallyGroup);
            Assert.Equal(_context.Groups.FirstOrDefault(el => el.Id == id), accuallyGroup);
        }



        [Fact]
        public void GetAllTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            //act
            List<Group> accually = repository.GetAll();
            //assert
            Assert.NotNull(accually);
            Assert.Equal(_context.Groups.Count(), accually.Count);
        }



        [Fact]
        public void GetAsNoTrackingExeptionLessThatZeroTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.GetAsNoTracking(0));

        }

        [Fact]
        public void GetAsNoTrackingExeptionOutOfRangeTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            //act
            //assert
            Assert.Throws<KeyNotFoundException>(() => repository.GetAsNoTracking(_context.Groups.Count() + 1));


        }

        [Fact]
        public void GetAsNoTrackingtTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            var id = _context.Groups.Count() - 1;
            //act
            Group accuallyGroup = repository.GetAsNoTracking(id);
            Group expected = _context.Groups.AsNoTracking().FirstOrDefault(el => el.Id == id);
            //assert
            Assert.NotNull(accuallyGroup);
            Assert.Equal(expected.Name, accuallyGroup.Name);
            Assert.Equal(expected.Description, accuallyGroup.Description);
            Assert.Equal(expected.Id, accuallyGroup.Id);
            Assert.Equal(expected.CourseId, accuallyGroup.CourseId);
        }



        [Fact]
        public void GetFilteredExeptionTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => repository.GetFiltered(null));
        }

        [Fact]
        public void GetFilteredTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);

            //act
            IQueryable<Group> accuallyGroups = repository.GetFiltered(expressions: e => e.CourseId == sameCourseIdForTest);
            IQueryable<Group> expected = _context.Groups.AsQueryable().Where(e => e.CourseId == sameCourseIdForTest);
            //assert
            Assert.NotNull(accuallyGroups);
            Assert.Equal(expected, accuallyGroups);
        }

        [Fact]
        public void InsertExeptionTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            Group group = null;
            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => repository.Insert(group));
        }

        [Fact]
        public void InsertTest()
        {
            //arange
            IRepository<Group> repository = new GroupRepository(_context);
            Group group = new Group() { Id = _context.Groups.Count() + 1, Name = "NameAdd", Description = "DescriptionAdd", CourseId = 1 };
            //act
            repository.Insert(group);
            Group accually = repository.Get(_context.Groups.Count());
            //assert
            Assert.NotNull(accually);
            Assert.Equal(group, accually);
        }


        [Fact]
        public void UpdateExeptionTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            Group group = null;
            //act

            //assert
            Assert.Throws<ArgumentNullException>(() => repository.Update(group));
        }

        [Fact]
        public void UpdateTest()
        {
            //arrange
            IRepository<Group> repository = new GroupRepository(_context);
            Group expectedGroup = repository.Get(_context.Groups.Count());
            expectedGroup.Name = "UpdatedName";
            //act
            repository.Update(expectedGroup);
            Group actualGroup = repository.GetAsNoTracking(_context.Groups.Count());
            //assert
            Assert.NotNull(actualGroup);
            Assert.Equal(expectedGroup.Name, actualGroup.Name);
        }
    }
}
