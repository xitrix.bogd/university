﻿using Domain;
using Moq;
using Repository;
using Service;
using Service.Realization;
using System.Linq.Expressions;

namespace ServiceTests
{
    public class StudentServiceTests
    {
        [Fact]
        public void StudentServiceNullExeptionTest()
        {
            //arrange
            IRepository<Student> repository = null;
            //act
            IStudentService studentService;
            //assert
            Assert.Throws<ArgumentNullException>(() => studentService = new StudentService(repository));
        }

        [Fact]
        public void AddStudentNullExeptionTest()
        {
            //arrange
            Student Student = null;
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => studentService.AddStudent(Student));

        }

        [Fact]
        public void AddStudentTest()
        {
            //arrange
            Student Student = new();
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            studentService.AddStudent(Student);
            //assert
            mockRepository.Verify(e => e.Insert(It.IsNotNull<Student>()), Times.Once);
        }



        [Fact]
        public void StudentExistNotValidIdTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            var accually = studentService.StudentExist(0);
            //assert
            Assert.Equal(false, accually);

        }

        [Fact]
        public void StudentExistValidIdTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            var accually = studentService.StudentExist(1);
            //assert
            mockRepository.Verify(e => e.GetFiltered(It.IsAny<Expression<Func<Student, bool>>>()), Times.Once);
        }


        [Fact]
        public void GetAllTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            var accually = studentService.GetAll();
            //assert
            mockRepository.Verify(e => e.GetAll(), Times.Once);
        }

        [Fact]
        public void GetStudentExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => studentService.GetStudent(0));

        }

        [Fact]
        public void GetStudentsByCourseExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => studentService.GetStudentsByGroup(0));

        }

        [Fact]
        public void GetStudentsByCourseTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            var accually = studentService.GetStudentsByGroup(1);
            //assert
            mockRepository.Verify(e => e.GetFiltered(It.IsAny<Expression<Func<Student, bool>>>()), Times.Once);
        }

        [Fact]
        public void GetStudentTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            studentService.GetStudent(1);
            //assert
            mockRepository.Verify(e => e.Get(It.IsAny<int>()), Times.Once);
        }


        [Fact]
        public void GetStudentThatNoTrackingExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => studentService.GetStudentThatNoTracking(0));

        }


        [Fact]
        public void GetStudentThatNoTrackingTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            studentService.GetStudentThatNoTracking(1);
            //assert
            mockRepository.Verify(e => e.GetAsNoTracking(It.IsAny<int>()), Times.Once);
        }


        [Fact]
        public void RemoveStudentExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => studentService.RemoveStudent(0));

        }


        [Fact]
        public void RemoveStudentTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            IStudentService studentService = new StudentService(mockRepository.Object);
            //act
            studentService.RemoveStudent(1);
            //assert
            mockRepository.Verify(e => e.Delete(It.IsAny<int>()), Times.Once);
        }



        [Fact]
        public void UpdateStudentExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            Student Student = null;
            IStudentService StudentService = new StudentService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => StudentService.UpdateStudent(Student));
        }


        [Fact]
        public void UpdateStudentTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Student>>();
            Student Student = new();
            IStudentService StudentService = new StudentService(mockRepository.Object);
            //act
            StudentService.UpdateStudent(Student);
            //assert
            mockRepository.Verify(e => e.Update(It.IsAny<Student>()), Times.Once);

        }
    }
}
