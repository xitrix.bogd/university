﻿using Domain;
using Moq;
using Repository;
using Service;
using Service.Realization;
using System.Linq.Expressions;

namespace ServiceTests
{
    public class GroupServiceTests
    {

        [Fact]
        public void GroupServiceNullExeptionTest()
        {
            //arrange
            IRepository<Group> repository = null;
            //act
            IGroupService GroupService;
            //assert
            Assert.Throws<ArgumentNullException>(() => GroupService = new GroupService(repository));
        }

        [Fact]
        public void AddGroupNullExeptionTest()
        {
            //arrange
            Group group = null;
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => GroupService.AddGroup(group));

        }

        [Fact]
        public void AddGroupTest()
        {
            //arrange
            Group group = new();
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            GroupService.AddGroup(group);
            //assert
            mockRepository.Verify(e => e.Insert(It.IsNotNull<Group>()), Times.Once);
        }



        [Fact]
        public void GroupExistNotValidIdTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            var accually = GroupService.GroupExist(0);
            //assert
            Assert.Equal(false, accually);

        }

        [Fact]
        public void GroupExistValidIdTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            var accually = GroupService.GroupExist(1);
            //assert
            mockRepository.Verify(e => e.GetFiltered(It.IsAny<Expression<Func<Group, bool>>>()), Times.Once);
        }


        [Fact]
        public void GetAllTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            var accually = GroupService.GetAll();
            //assert
            mockRepository.Verify(e => e.GetAll(), Times.Once);
        }

        [Fact]
        public void GetGroupExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => GroupService.GetGroup(0));

        }

        [Fact]
        public void GetGroupsByCourseExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => GroupService.GetGroupsByCourse(0));

        }

        [Fact]
        public void GetGroupsByCourseTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            var accually = GroupService.GetGroupsByCourse(1);
            //assert
            mockRepository.Verify(e => e.GetFiltered(It.IsAny<Expression<Func<Group, bool>>>()), Times.Once);
        }

        [Fact]
        public void GetGroupTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            GroupService.GetGroup(1);
            //assert
            mockRepository.Verify(e => e.Get(It.IsAny<int>()), Times.Once);
        }


        [Fact]
        public void GetGroupThatNoTrackingExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => GroupService.GetGroupThatNoTracking(0));

        }


        [Fact]
        public void GetGroupThatNoTrackingTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            GroupService.GetGroupThatNoTracking(1);
            //assert
            mockRepository.Verify(e => e.GetAsNoTracking(It.IsAny<int>()), Times.Once);
        }


        [Fact]
        public void RemoveGroupExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => GroupService.RemoveGroup(0));

        }


        [Fact]
        public void RemoveGroupTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            GroupService.RemoveGroup(1);
            //assert
            mockRepository.Verify(e => e.Delete(It.IsAny<int>()), Times.Once);
        }



        [Fact]
        public void UpdateGroupExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            Group Group = null;
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => GroupService.UpdateGroup(Group));
        }


        [Fact]
        public void UpdateGroupTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Group>>();
            Group Group = new();
            IGroupService GroupService = new GroupService(mockRepository.Object);
            //act
            GroupService.UpdateGroup(Group);
            //assert
            mockRepository.Verify(e => e.Update(It.IsAny<Group>()), Times.Once);

        }

    }
}
