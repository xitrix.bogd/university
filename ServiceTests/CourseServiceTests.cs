using Domain;
using Moq;
using Repository;
using Service;
using Service.Realization;
using System.Linq.Expressions;


namespace ServiceTests
{
    public class CourseServiceTests
    {
        [Fact]
        public void CourseServiceNullExeptionTest()
        {
            //arrange
            IRepository<Course> repository = null;
            //act
            ICourseService courseService;
            //assert
            Assert.Throws<ArgumentNullException>(() => courseService = new CourseService(repository));
        }

        [Fact]
        public void AddCourseNullExeptionTest()
        {
            //arrange
            Course course = null;
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => courseService.AddCourse(course));

        }

        [Fact]
        public void AddCourseTest()
        {
            //arrange
            Course course = new();
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            courseService.AddCourse(course);
            //assert
            mockRepository.Verify(e => e.Insert(It.IsNotNull<Course>()), Times.Once);
        }



        [Fact]
        public void CourseExistNotValidIdTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            var accually = courseService.CourseExist(0);
            //assert
            Assert.Equal(false, accually);

        }

        [Fact]
        public void CourseExistValidIdTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            var accually = courseService.CourseExist(1);
            //assert
            mockRepository.Verify(e => e.GetFiltered(It.IsAny<Expression<Func<Course, bool>>>()), Times.Once);
        }


        [Fact]
        public void GetAllTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            var accually = courseService.GetAll();
            //assert
            mockRepository.Verify(e => e.GetAll(), Times.Once);
        }

        [Fact]
        public void GetCourseExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => courseService.GetCourse(0));

        }


        [Fact]
        public void GetCourseTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            courseService.GetCourse(1);
            //assert
            mockRepository.Verify(e => e.Get(It.IsAny<int>()), Times.Once);
        }


        [Fact]
        public void GetCourseThatNoTrackingExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => courseService.GetCourseThatNoTracking(0));

        }


        [Fact]
        public void GetCourseThatNoTrackingTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            courseService.GetCourseThatNoTracking(1);
            //assert
            mockRepository.Verify(e => e.GetAsNoTracking(It.IsAny<int>()), Times.Once);
        }


        [Fact]
        public void RemoveCourseExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => courseService.RemoveCourse(0));

        }


        [Fact]
        public void RemoveCourseTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            courseService.RemoveCourse(1);
            //assert
            mockRepository.Verify(e => e.Delete(It.IsAny<int>()), Times.Once);
        }



        [Fact]
        public void UpdateCourseExeptionTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            Course course = null;
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            //assert
            Assert.Throws<ArgumentNullException>(() => courseService.UpdateCourse(course));
        }


        [Fact]
        public void UpdateCourseTest()
        {
            //arrange
            var mockRepository = new Mock<IRepository<Course>>();
            Course course = new();
            ICourseService courseService = new CourseService(mockRepository.Object);
            //act
            courseService.UpdateCourse(course);
            //assert
            mockRepository.Verify(e => e.Update(It.IsAny<Course>()), Times.Once);

        }





    }
}