﻿using Domain;

namespace Service
{
    public interface ICourseService
    {
        void AddCourse(Course course);
        void RemoveCourse(int id);
        void UpdateCourse(Course course);
        List<Course> GetAll();
        Course GetCourse(int id);
        Course GetCourseThatNoTracking(int id);
        bool CourseExist(int id);
    }
}
