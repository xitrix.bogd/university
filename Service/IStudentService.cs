﻿using Domain;

namespace Service
{
    public interface IStudentService
    {
        List<Student> GetStudentsByGroup(int groupId);
        void AddStudent(Student student);
        void RemoveStudent(int id);
        void UpdateStudent(Student student);
        List<Student> GetAll();
        Student GetStudent(int id);
        Student GetStudentThatNoTracking(int id);
        bool StudentExist(int id);
    }
}
