﻿using Domain;
using Repository;

namespace Service.Realization
{
    public class GroupService : IGroupService
    {
        private readonly IRepository<Group> _groupRepository;

        public GroupService(IRepository<Group> repository)
        {
            _groupRepository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public void AddGroup(Group group)
        {
            if (group == null)
            {
                throw new ArgumentNullException(nameof(group));
            }
            _groupRepository.Insert(group);
        }

        public List<Group> GetAll()
        {
            return _groupRepository.GetAll();
        }

        public Group GetGroup(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            return _groupRepository.Get(id);
        }

        public List<Group> GetGroupsByCourse(int courseId)
        {
            if (courseId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(courseId));
            }
            return _groupRepository.GetFiltered(expressions: s => s.CourseId == courseId).ToList();
        }

        public Group GetGroupThatNoTracking(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            return _groupRepository.GetAsNoTracking(id);
        }

        public bool GroupExist(int id)
        {
            if (id <= 0)
            {
                return false;
            }
            return _groupRepository.GetFiltered(expressions: s => s.Id == id).Any();
        }

        public void RemoveGroup(int id)
        {
            if (id <= 0)
            {
               throw new ArgumentOutOfRangeException(nameof(id));
            }
            _groupRepository.Delete(id);

        }

        public void UpdateGroup(Group group)
        {
            if (group == null)
            {
                throw new ArgumentNullException(nameof(group));
            }
            _groupRepository.Update(group);
        }
    }
}
