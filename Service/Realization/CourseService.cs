﻿using Domain;
using Repository;

namespace Service.Realization
{
    public class CourseService : ICourseService
    {

        private readonly IRepository<Course> _courseRepository;

        public CourseService(IRepository<Course> courseRepository)
        {
            _courseRepository = courseRepository ?? throw new ArgumentNullException(nameof(courseRepository));
        }

        public void AddCourse(Course course)
        {

            if (course == null)
            {
                throw new ArgumentNullException(nameof(course));
            }
            _courseRepository.Insert(course);
        }

        public bool CourseExist(int id)
        {
            if (id <= 0)
            {
                return false;
            }
            return _courseRepository.GetFiltered(expressions: s => s.Id == id).Any();
        }

        public List<Course> GetAll()
        {
            return _courseRepository.GetAll();
        }

        public Course GetCourse(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            return _courseRepository.Get(id);
        }

        public Course GetCourseThatNoTracking(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            return _courseRepository.GetAsNoTracking(id);
        }

        public void RemoveCourse(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _courseRepository.Delete(id);
        }

        public void UpdateCourse(Course course)
        {
            if (course == null)
            {
                throw new ArgumentNullException(nameof(course));
            }
            _courseRepository.Update(course);
        }
    }
}
