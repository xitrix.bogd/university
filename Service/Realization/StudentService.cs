﻿using Domain;
using Repository;

namespace Service.Realization
{
    public class StudentService : IStudentService
    {
        private readonly IRepository<Student> _repository;

        public StudentService(IRepository<Student> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public void AddStudent(Student student)
        {
            if (student == null)
            {
                throw new ArgumentNullException(nameof(student));
            }
            _repository.Insert(student);
        }

        public List<Student> GetAll()
        {
            return _repository.GetAll();
        }

        public Student GetStudent(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            return _repository.Get(id);
        }

        public List<Student> GetStudentsByGroup(int groupId)
        {
            if (groupId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(groupId));
            }
            return _repository.GetFiltered(expressions: s => s.GroupId == groupId).ToList();
        }

        public Student GetStudentThatNoTracking(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            return _repository.GetAsNoTracking(id);
        }

        public void RemoveStudent(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _repository.Delete(id);
        }

        public bool StudentExist(int id)
        {
            if (id <= 0)
            {
                return false;
            }
            return _repository.GetFiltered(expressions: s => s.Id == id).Any();
        }

        public void UpdateStudent(Student student)
        {
            if (student == null)
            {
                throw new ArgumentNullException(nameof(student));
            }
            _repository.Update(student);
        }
    }
}
