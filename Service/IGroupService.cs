﻿using Domain;

namespace Service
{
    public interface IGroupService
    {
        List<Group> GetGroupsByCourse(int courseId);
        void AddGroup(Group group);
        void RemoveGroup(int id);
        void UpdateGroup(Group group);
        List<Group> GetAll();
        Group GetGroup(int id);
        Group GetGroupThatNoTracking(int id);
        bool GroupExist(int id);
    }
}
